/* latte-card-view.h
 *
 * Copyright 2021 KonstantIMP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#if !defined(LIBLATTE_INSIDE) && !defined(LIBLATTE_COMPILATION)
#error "Only <latte.h> can be included directly."
#endif

#include "latte-version.h"

#include <gtk/gtk.h>
#include <glib.h>

G_BEGIN_DECLS

#define LATTE_TYPE_CARDVIEW (latte_card_view_get_type())

G_DECLARE_FINAL_TYPE (LatteCardView, latte_card_view, LATTE, CARDVIEW, GtkWidget)

LATTE_AVAILABLE_IN_ALL
GtkWidget *latte_card_view_new (void) G_GNUC_WARN_UNUSED_RESULT;

LATTE_AVAILABLE_IN_ALL
gulong latte_card_view_get_border_radius (LatteCardView *);
LATTE_AVAILABLE_IN_ALL
void latte_card_view_set_border_radius (LatteCardView *, gulong);

LATTE_AVAILABLE_IN_ALL
void latte_card_view_remove_child (LatteCardView *);
LATTE_AVAILABLE_IN_ALL
GtkWidget *latte_card_view_get_child (LatteCardView *);
LATTE_AVAILABLE_IN_ALL
void latte_card_view_set_child (LatteCardView *, GtkWidget *);

G_END_DECLS

