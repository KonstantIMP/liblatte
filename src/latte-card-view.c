/* latte-card-view.c
 *
 * Copyright 2021 KonstantIMP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "latte-card-view.h"

/**
 * LatteCardView:
 *
 * A container with rounded corners and colored background
 *
 * 'LatteCardView' create a card and show child inside
 *
 * 'LatteCardView' rounds corners getting [property@Latte.CardView:border-radius]
 *
 * ## CSS nodes
 *
 * 'LatteCardView' has a single CSS node with name 'cardview'.
 *
 * Since: 0.1.0
 */
struct _LatteCardView
{
  GtkWidget parent_instance;

  GtkWidget* child;

  gulong border_radius;
};

G_DEFINE_TYPE (LatteCardView, latte_card_view, GTK_TYPE_WIDGET)

enum
{
  PROP_0,
  PROP_BORDER_RADIUS,
  PROP_CHILD,
  PROP_LAST_PROP,
};
static GParamSpec *props[PROP_LAST_PROP];

static void
latte_card_view_get_property (GObject    *object,
                              guint      property_id,
                              GValue     *value,
                              GParamSpec *pspec)
{
  LatteCardView *self = LATTE_CARDVIEW (object);

  switch (property_id) {
  case PROP_BORDER_RADIUS:
    g_value_set_ulong (value, self->border_radius);
    break;

  case PROP_CHILD:
    g_value_set_object (value, G_OBJECT (self->child));
    break;

  default:
    G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
    break;
  }
}

static void
latte_card_view_set_property (GObject      *object,
                              guint         property_id,
                              const GValue *value,
                              GParamSpec   *pspec)
{
  LatteCardView *self = LATTE_CARDVIEW (object);
  switch (property_id) {
  case PROP_BORDER_RADIUS:
    latte_card_view_set_border_radius (self, g_value_get_ulong (value));
    break;

  case PROP_CHILD:
    latte_card_view_set_child (self, GTK_WIDGET (g_value_get_object (value)));
    break;

  default:
    G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
    break;
  }
}

static void
latte_card_view_dispose (GObject *object)
{
  LatteCardView *self = LATTE_CARDVIEW (object);
  GtkWidget *child;

  if ((child = gtk_widget_get_first_child (GTK_WIDGET (self)))) {
    gtk_widget_unparent (child);
  }

  G_OBJECT_CLASS (latte_card_view_parent_class)->dispose (object);
}

static void
latte_card_view_finalize (GObject *object)
{
  LatteCardView *self = LATTE_CARDVIEW (object);

  self->child = NULL;

  G_OBJECT_CLASS (latte_card_view_parent_class)->finalize (object);
}

static void
latte_card_view_snapshot (GtkWidget   *widget,
                          GtkSnapshot *snapshot)
{
  LatteCardView *self = LATTE_CARDVIEW (widget);

  GdkRGBA red, green, yellow, blue;
  float w, h;

  gdk_rgba_parse (&red, "red");
  gdk_rgba_parse (&green, "green");
  gdk_rgba_parse (&yellow, "yellow");
  gdk_rgba_parse (&blue, "blue");

  w = gtk_widget_get_width (widget) / 2.0;
  h = gtk_widget_get_height (widget) / 2.0;

  gtk_snapshot_append_color (snapshot, &red,
                             &GRAPHENE_RECT_INIT(0, 0, w, h));
  gtk_snapshot_append_color (snapshot, &green,
                             &GRAPHENE_RECT_INIT(w, 0, w, h));
  gtk_snapshot_append_color (snapshot, &yellow,
                             &GRAPHENE_RECT_INIT(0, h, w, h));
  gtk_snapshot_append_color (snapshot, &blue,
                             &GRAPHENE_RECT_INIT(w, h, w, h));

  gtk_widget_snapshot_child (widget, self->child, snapshot);
}

static void
latte_card_view_class_init (LatteCardViewClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (klass);

  object_class->dispose = latte_card_view_dispose;
  object_class->finalize = latte_card_view_finalize;
  object_class->set_property = latte_card_view_set_property;
  object_class->get_property = latte_card_view_get_property;

  widget_class->snapshot = latte_card_view_snapshot;

  /**
   * LatteCardView:border-radius: (attributes org.gtk.Property.get=latte_card_view_get_border_radius org.gtk.Property.set=latte_card_view_set_border_radius)
   *
   * Border raius for corners rounding
   *
   * Since: 0.1.0
   */
  props[PROP_BORDER_RADIUS] =
    g_param_spec_ulong ("border-radius",
                        "Border radius",
                        "Border radius for the card",
                        0,
                        30,
                        15,
                        G_PARAM_READWRITE);

  /**
   * LatteCardView:child: (attributes org.gtk.Property.get=latte_card_view_get_child org.gtk.Property.set=latte_card_view_set_child)
   *
   * Child fordisplaing inside the card
   *
   * Since: 0.1.0
   */
  props[PROP_CHILD] =
    g_param_spec_object ("child",
                         "Child",
                         "The child of the card",
                         GTK_TYPE_WIDGET,
                         G_PARAM_READWRITE);

  g_object_class_install_properties (object_class, PROP_LAST_PROP, props);

  gtk_widget_class_set_layout_manager_type (widget_class, GTK_TYPE_BIN_LAYOUT);
}

static void
latte_card_view_init (LatteCardView *self)
{
  self->border_radius = 15;
  self->child = NULL;
}

/**
 * latte_card_view_new:
 *
 * Creates a new 'LatteCardView'
 *
 * Returns: the new created 'LatteCardView'
 *
 * Since: 0.1.0
 */
GtkWidget *
latte_card_view_new (void)
{
  return g_object_new (LATTE_TYPE_CARDVIEW, NULL);
}

/**
 * latte_card_view_get_border_radius: (attributes org.gtk.Method.get_property=border-radius)
 * @self: a `LatteCardView`
 *
 * Gets the radius of corners.
 *
 * Returns: the current border radius of @self
 *
 * Since: 0.1.0
 */
gulong
latte_card_view_get_border_radius (LatteCardView *self)
{
  g_return_val_if_fail (LATTE_IS_CARDVIEW (self), 0);

  return self->border_radius;
}

/**
 * latte_card_view_set_border_radius: (attributes org.gtk.Method.set_property=border-radius)
 * @self: a `LatteCardView`
 * @border_radius: new value for 'LatteCardView:border_radius'
 *
 * Sets border radius for @self
 *
 * Since: 0.1.0
 */
void
latte_card_view_set_border_radius (LatteCardView *self,
                                   gulong        border_radius)
{
  g_return_if_fail (LATTE_IS_CARDVIEW (self));

  if (self->border_radius == border_radius)
    return;

  self->border_radius = border_radius;
  g_object_notify_by_pspec (G_OBJECT (self),
                            props[PROP_BORDER_RADIUS]);
}

/**
 * 'latte_card_view_remove_child'
 * @self: a 'LatteCardView'
 *
 * Removes a child widget from @self.
 *
 * The child must have been added before with
 * [method@LatteCardView.set_child]
 *
 * Since: 0.1.0
 */
void
latte_card_view_remove_child (LatteCardView *self)
{
  g_return_if_fail (LATTE_IS_CARDVIEW (self));
  g_return_if_fail (GTK_IS_WIDGET (self->child));
  g_return_if_fail (gtk_widget_get_parent (self->child) == (GtkWidget *)self);

  gtk_widget_unparent (self->child);
  self->child = NULL;
}

/**
 * latte_card_view_get_child: (attributes org.gtk.Method.get_property=child)
 * @self: a `LatteCardView`
 *
 * Gets the cards' child.
 *
 * Returns: the child of @self
 *
 * Since: 0.1.0
 */
GtkWidget *
latte_card_view_get_child (LatteCardView *self)
{
  g_return_val_if_fail (LATTE_IS_CARDVIEW (self), NULL);
  g_return_val_if_fail (GTK_IS_WIDGET (self->child), NULL);
  g_return_val_if_fail (gtk_widget_get_parent (self->child) == (GtkWidget *)self, NULL);

  return self->child;
}

/**
 * latte_card_view_set_child: (attributes org.gtk.Method.set_property=child)
 * @self: a `LatteCardView`
 * @child: New childforthe card
 *
 * Sets the cards' child.
 *
 * Since: 0.1.0
 */
void
latte_card_view_set_child (LatteCardView *self,
                           GtkWidget     *child)
{
  g_return_if_fail (LATTE_IS_CARDVIEW (self));
  g_return_if_fail (GTK_IS_WIDGET (child));
  g_return_if_fail (gtk_widget_get_parent (child) == NULL);

  gtk_widget_insert_before (child, GTK_WIDGET (self), NULL);

  self->child = child;
}

