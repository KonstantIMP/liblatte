#include <gtk/gtk.h>
#include <latte.h>

static void
activate (GtkApplication* app,
          gpointer        user_data)
{
  GtkWidget *win = gtk_application_window_new (app);
  gtk_window_set_title (GTK_WINDOW (win), "Example 1. Liblatte");
  gtk_window_set_default_size (GTK_WINDOW (win), 300, 300);

  GtkWidget *card = latte_card_view_new ();
  latte_card_view_set_border_radius (LATTE_CARDVIEW (card), 15);
  latte_card_view_set_child (LATTE_CARDVIEW (card), gtk_label_new ("Woof"));

  gtk_window_set_child (GTK_WINDOW (win), card);

  gtk_widget_show (win);
}

int
main (int  argc,
      char ** argv)
{
  GtkApplication *app = gtk_application_new ("org.latte.cardview.test.kimp", G_APPLICATION_FLAGS_NONE);
  g_signal_connect (app, "activate", G_CALLBACK (activate), NULL);
  int status = g_application_run (G_APPLICATION (app), argc, argv);
  g_object_unref (app);
  return status;
}
